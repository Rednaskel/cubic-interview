package pages;

import driver.Driver;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserHomePage {

    @Autowired
    Driver driver;

    private final By PROFILE_BUTTON_BY = By.id("header-profile-button");

    public UserHomePage() {

    }

    UserHomePage(Driver driver) {
        this.driver = driver;
    }

    public String loggedUserName() {
        return driver.findVisibleElement(PROFILE_BUTTON_BY).getText();
    }
}
