package pages;

import driver.Driver;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoginPage {

    @Autowired
    private Driver driver;

    private final String URL = "https://login.yahoo.com/config/login";

    private final By USERNAME_BY = By.id("login-username");
    private final By NEXT_BUTTON_BY = By.id("login-signin");
    private final By PASSWORD_BY = By.id("login-passwd");
    private final By FORGOT_PASSWORD_BY = By.id("mbr-forgot-link");

    public LoginPage loadPage() {
        driver.goTo(URL);
        return this;
    }

    public LoginPage enterLogin(String login) {
        driver.sendKeys(USERNAME_BY, login);
        return this;
    }

    public LoginPage enterPassword(String password) {
        driver.sendKeys(PASSWORD_BY, password);
        return this;
    }

    public LoginPage pressNextButton() {
        driver.click(NEXT_BUTTON_BY);
        return this;
    }

    public ResetPasswordPage pressForgotpassword() {
        driver.click(FORGOT_PASSWORD_BY);
        return new ResetPasswordPage(driver);
    }

    public UserHomePage pressLoginButton() {
        driver.click(NEXT_BUTTON_BY);
        return new UserHomePage(driver);
    }

}
