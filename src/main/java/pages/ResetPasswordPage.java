package pages;

import driver.Driver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ResetPasswordPage {

    @Autowired
    private Driver driver;

    public ResetPasswordPage() {

    }

    public ResetPasswordPage(Driver driver) {
        this.driver = driver;
    }
}
