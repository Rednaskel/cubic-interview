package rules;

import driver.Driver;
import org.apache.log4j.Logger;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.logging.LogEntries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BrowserLogsRule extends TestWatcher {

    private static Logger logger = Logger.getLogger(BrowserLogsRule.class);

    @Autowired
    private Driver driver;

    @Override
    public void failed(Throwable e, Description description) {
        LogEntries logs = driver.getBrowserLogs();
        logs.forEach(logger::info);
    }



}
