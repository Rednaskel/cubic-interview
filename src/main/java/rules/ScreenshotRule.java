package rules;

import driver.Driver;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Component
public class ScreenshotRule extends TestWatcher {

    @Autowired
    private Driver driver;

    @Override
    public void failed(Throwable e, Description description) {

        String folderName = "screenshots";
        createScreenshotsFolder(folderName);

        byte[] screenshot = driver.takeScreenshot();
        String screenshotName = description.getClassName() +
                "_" +
                description.getMethodName() +
                ".png";
        try {
            saveScreenshot(screenshot, folderName + "/" + screenshotName);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void finished(Description description) {
        driver.quit();
    }

    private void saveScreenshot(byte[] screenshot, String screenshotPath) throws IOException {
        FileOutputStream out = new FileOutputStream(screenshotPath);
        out.write(screenshot);
    }

    private void createScreenshotsFolder(String folderName) {
        File folderForScreenshots = new File(folderName);
        folderForScreenshots.mkdir();
    }

}