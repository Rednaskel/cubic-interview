package driver;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;

class DriverConfigurator {

    private static final int DEFAULT_HEIGHT = 1200;
    private static final int DEFAULT_WIDTH = 800;

    static void useDefaultConfiguration(WebDriver driver) {
        driver.manage().window().setSize(new Dimension(DEFAULT_HEIGHT, DEFAULT_WIDTH));
    }

    static ChromeOptions getChromeOptions() {
        ChromeOptions options = new ChromeOptions();
        if (System.getProperty("headless") != null) {
            options.setHeadless(true);
        }
        return options;
    }

    static FirefoxOptions getFirefoxOptions() {
        FirefoxOptions options = new FirefoxOptions();
        if (System.getProperty("headless") != null) {
            options.setHeadless(true);
        }
        return options;
    }
}
