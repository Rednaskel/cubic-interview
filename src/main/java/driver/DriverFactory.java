package driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverFactory {

    static RemoteWebDriver createDriver() {
        String browserName = System.getProperty("browser", "chrome");
        switch (browserName) {
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                return new FirefoxDriver(DriverConfigurator.getFirefoxOptions());
            default:
                WebDriverManager.chromedriver().setup();
                return new ChromeDriver(DriverConfigurator.getChromeOptions());
        }
    }

}
