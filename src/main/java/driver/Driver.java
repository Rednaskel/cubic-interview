package driver;

import org.openqa.selenium.*;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Component;

@Component
public class Driver {

    private WebDriver driver;
    private final int TIMEOUT = 15;
    private WebDriverWait wait;

    public WebElement findVisibleElement(By by) {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public WebElement findClickableElement(By by) {
        return wait.until(ExpectedConditions.elementToBeClickable(by));
    }

    public void sendKeys(By by, CharSequence... text) {
        findClickableElement(by).sendKeys(text);
    }

    public void click(By by) {
        findClickableElement(by).click();
    }

    public void goTo(String url) {
        if (driverClosed()) {
            initializeDriver();
        }
        driver.get(url);
    }

    private boolean driverClosed() {
        return driver == null || driver.toString().contains("null");
    }

    public void quit() {
        driver.quit();
    }

    public byte[] takeScreenshot() {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }


    private void initializeDriver() {
        driver = DriverFactory.createDriver();
        DriverConfigurator.useDefaultConfiguration(driver);
        wait = new WebDriverWait(driver, TIMEOUT);
    }

    public LogEntries getBrowserLogs() {
        return driver.manage().logs().get("browser");
    }

}
