package tests;

import config.SpringConfig;
import org.junit.Rule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import rules.BrowserLogsRule;
import rules.ScreenshotRule;

@ContextConfiguration(classes = SpringConfig.class)
public class TestTemplate {

    @Autowired
    @Rule
    public ScreenshotRule screenshotRule;

    @Autowired
    @Rule
    public BrowserLogsRule browserLogsRule;

}
