package login;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pages.LoginPage;
import pages.UserHomePage;
import tests.TestTemplate;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class LoginTest extends TestTemplate {

    @Autowired
    LoginPage loginPage;

    @Test
    public void shouldLoignSuccessfully() {

        //given
        final String user = "cubic";
        final String login = "cubic_interview_12345@yahoo.com";
        final String password = "exampletask";

        loginPage.loadPage();

        //when
        UserHomePage userHomePage = loginPage.enterLogin(login)
                .pressNextButton()
                .enterPassword(password)
                .pressLoginButton();

        //then
        assertThat(userHomePage.loggedUserName()).as("Expected page did not load").isEqualTo(user);
    }
}
